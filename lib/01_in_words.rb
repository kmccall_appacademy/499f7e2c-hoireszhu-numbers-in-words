class Fixnum

  def in_words
    numbers_to_words = {
      1_000_000_000_000 => "trillion",
      1_000_000_000 => "billion",
      1_000_000 => "million",
      1000 => "thousand",
      100 => "hundred",
      90 => "ninety",
      80 => "eighty",
      70 => "seventy",
      60 => "sixty",
      50 => "fifty",
      40 => "forty",
      30 => "thirty",
      20 => "twenty",
      19 => "nineteen",
      18 => "eighteen",
      17 => "seventeen",
      16 => "sixteen",
      15 => "fifteen",
      14 => "fourteen",
      13 => "thirteen",
      12 => "twelve",
      11 => "eleven",
      10 => "ten",
      9 => "nine",
      8 => "eight",
      7 => "seven",
      6 => "six",
      5 => "five",
      4 => "four",
      3 => "three",
      2 => "two",
      1 => "one"
    }

    return numbers_to_words[self] if numbers_to_words.has_key?(self) && self<100
    return "zero" if self == 0

    count = {}
    count["trillion"]=self/1_000_000_000_000
    count["billion"]=self%1_000_000_000_000/1_000_000_000
    count["million"]=self%1_000_000_000/1_000_000
    count["thousand"]=self%1_000_000/1000

    result = []

    if self<1000
      return numbers_to_words_under_thousand(self)
    else
      result << numbers_to_words_under_thousand(self.to_s[-3..-1].to_i) if self.to_s[-3..-1].to_i != 0

      if count["thousand"] != 0
        result.unshift("thousand")
        result.unshift(numbers_to_words_under_thousand(count["thousand"]))
      end

      if count["million"] != 0
        result.unshift("million")
        result.unshift(numbers_to_words_under_thousand(count["million"]))
      end

      if count["billion"] != 0
        result.unshift("billion")
        result.unshift(numbers_to_words_under_thousand(count["billion"]))
      end

      if count["trillion"] != 0
        result.unshift("trillion")
        result.unshift(numbers_to_words_under_thousand(count["trillion"]))
      end

    end

    result.join(" ")
  end

  private

  def numbers_to_words_under_thousand(num)
    numbers_to_words = {
      100 => "hundred",
      90 => "ninety",
      80 => "eighty",
      70 => "seventy",
      60 => "sixty",
      50 => "fifty",
      40 => "forty",
      30 => "thirty",
      20 => "twenty",
      19 => "nineteen",
      18 => "eighteen",
      17 => "seventeen",
      16 => "sixteen",
      15 => "fifteen",
      14 => "fourteen",
      13 => "thirteen",
      12 => "twelve",
      11 => "eleven",
      10 => "ten",
      9 => "nine",
      8 => "eight",
      7 => "seven",
      6 => "six",
      5 => "five",
      4 => "four",
      3 => "three",
      2 => "two",
      1 => "one"
    }

    return numbers_to_words[num] if numbers_to_words.has_key?(num) && num<100
    result = []

    if numbers_to_words.has_key?(num.to_s[-2..-1].to_i)
      result << numbers_to_words[num.to_s[-2..-1].to_i]
    else
      result << numbers_to_words[(num.to_s[-2]+"0").to_i]
      result << numbers_to_words[num.to_s[-1].to_i] if num.to_s[-1].to_i != 0
    end


    if num/100 > 0
      result.unshift("hundred")
      result.unshift(numbers_to_words[num/100])
    end

    result.join(" ").rstrip #get rid of empty spaces for numbers like 100, 1000
  end

end
